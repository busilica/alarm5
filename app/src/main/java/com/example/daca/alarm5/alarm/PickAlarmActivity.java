package com.example.daca.alarm5.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TimePicker;

import com.example.daca.alarm5.R;
import com.example.daca.alarm5.alarm.service.AlarmReceiver;

import java.util.Calendar;

public class PickAlarmActivity extends AppCompatActivity implements View.OnClickListener {

    FloatingActionButton fabCancel, fabOk;

    TimePicker timePicker;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_alarm);
        timePicker = (TimePicker) findViewById(R.id.timePicker_pick_alarm_activity);
        timePicker.setIs24HourView(true);
        calendar = Calendar.getInstance();
        fabCancel = (FloatingActionButton) findViewById(R.id.fab_cancel_pick_alarm_activity);
        fabCancel.setOnClickListener(this);
        fabOk = (FloatingActionButton) findViewById(R.id.fab_ok_pick_alarm_activity);
        fabOk.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fab_cancel_pick_alarm_activity:
                setResult(0);
                finish();
                break;
            case R.id.fab_ok_pick_alarm_activity:
                calendar.set(Calendar.HOUR_OF_DAY, timePicker.getHour());
                calendar.set(Calendar.MINUTE, timePicker.getMinute());
                setResult(2, new Intent().putExtra("alarmHour",   timePicker.getHour())
                                         .putExtra("alarmMinute", timePicker.getMinute())
                                         .putExtra("timeInMillis", calendar.getTimeInMillis()));
                timePicker = null;
                calendar = null;
                finish();
                break;
        }
    }
}
