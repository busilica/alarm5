package com.example.daca.alarm5.alarm.database;

/**
 * Created by Daca on 16.03.2017..
 */

public class Alarm {

    private int id;
    private int clockHour;
    private int clockMinute;
    private long mili;
    private int onOff; //0 - Off, 1 - On

    public Alarm(){

    }

    public Alarm(int id, int clockHour, int clockMinute, long mili, int onOff) {
        this.id = id;
        this.clockHour = clockHour;
        this.clockMinute = clockMinute;
        this.mili = mili;
        this.onOff = onOff;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClockHour() {
        return clockHour;
    }

    public void setClockHour(int clockHour) {
        this.clockHour = clockHour;
    }

    public int getClockMinute() {
        return clockMinute;
    }

    public void setClockMinute(int clockMinute) {
        this.clockMinute = clockMinute;
    }

    public long getMili() {
        return mili;
    }

    public void setMili(long mili) {
        this.mili = mili;
    }

    public int getOnOff() {
        return onOff;
    }

    public void setOnOff(int onOff) {
        this.onOff = onOff;
    }
}
