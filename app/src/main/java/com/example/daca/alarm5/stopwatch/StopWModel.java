package com.example.daca.alarm5.stopwatch;

import android.os.Handler;
import android.os.SystemClock;

/**
 * Created by Daca on 18.03.2017..
 */

public class StopWModel implements Runnable, Contract.StopWModel {

    private Contract.StopWPresenter presenter;

    private long milliTime, timeBuff, updateTime = 0L;

    private Handler handler;

    private int seconds, minutes, milliseconds;

    public StopWModel(Contract.StopWPresenter presenter) {
        this.presenter = presenter;
        handler = new Handler();
    }

    @Override
    public void run() {
        milliTime = SystemClock.uptimeMillis() - presenter.getStartTime();
        updateTime = timeBuff + milliTime;
        seconds = (int) (updateTime / 1000);
        minutes = seconds / 60;
        milliseconds = (int) (updateTime % 1000);
        String sMinutes = String.valueOf(minutes);
        if (seconds >= 60){
            seconds -= 60;
        }
        String sSeconds = String.format("%02d", seconds);
        String sMilliseconds = String.format("%03d", milliseconds);
        presenter.passCountedValues(sMinutes, sSeconds, sMilliseconds);
        handler.postDelayed(this, 0);
    }

    @Override
    public void resetStopWatch() {
        milliTime = 0L;
        timeBuff = 0L;
        updateTime = 0L;
        seconds = 0;
        minutes = 0;
    }

    @Override
    public void startStopWatch() {
        handler.postDelayed(this, 0);
    }

    @Override
    public void pauseStopWatch() {
        timeBuff += milliTime;
        handler.removeCallbacks(this);
    }
}
