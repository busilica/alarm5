package com.example.daca.alarm5.alarm.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.example.daca.alarm5.alarm.DismissAlarmActivity;


/**
 * Created by Daca on 14.03.2017..
 */

public class AlarmReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        //context.startActivity(new Intent(context, DismissAlarmActivity.class));

        //fetch extra Strings from the intent
        String stringExtra = intent.getExtras().getString("extra");


        //create an intent to the ringtone service
        Intent serviceIntent = new Intent(context, RingtonePlayingService.class);

        //pass the extra String from MainActivity to the RingtonePlayingService
        serviceIntent.putExtra("extra", stringExtra);

        //start ringtone service
        context.startService(serviceIntent);
    }
}
