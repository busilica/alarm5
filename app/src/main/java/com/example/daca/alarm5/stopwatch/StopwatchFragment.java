package com.example.daca.alarm5.stopwatch;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.daca.alarm5.R;

import java.util.ArrayList;

/**
 * Created by Daca on 15.03.2017..
 */

public class StopwatchFragment extends Fragment implements Contract.StopWView {

    TextView minText;
    TextView secText;
    TextView milliText;
    FloatingActionButton startPause, reset, lap;
    private Contract.StopWPresenter presenter;
    private Contract.StopWModel model;
    private ListView listView;
    private ArrayList<String> listElementsArrayList;
    private ArrayAdapter<String> adapter;


    public static StopwatchFragment newInstance(){
        return new StopwatchFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_stopwatch, container, false);
        minText = (TextView) fragmentView.findViewById(R.id.txt_min_fragment_stopwatch);
        secText = (TextView) fragmentView.findViewById(R.id.txt_sec_fragment_stopwatch);
        milliText = (TextView) fragmentView.findViewById(R.id.txt_milli_fragment_stopwatch);
        startPause = (FloatingActionButton) fragmentView.findViewById(R.id.fab_start_fragment_stopwatch);
        reset = (FloatingActionButton) fragmentView.findViewById(R.id.fab_reset);
        lap = (FloatingActionButton) fragmentView.findViewById(R.id.fab_add_lap);
        presenter = new StopWPresenter(this);
        model = new StopWModel(presenter);
        presenter.setModel(model);
        startPause.setTag(0);
        reset.setTag(2);
        lap.setTag(3);
        lap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.startStop((Integer) v.getTag());
            }
        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.startStop((Integer) v.getTag());
            }
        });
        listView = (ListView) fragmentView.findViewById(R.id.list_view);
        listElementsArrayList = new ArrayList<>();
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listElementsArrayList);
        listView.setAdapter(adapter);
        startPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.startStop((Integer) v.getTag());
            }
        });
        return fragmentView;
    }

    @Override
    public void setupFAB(int tag, int imgId) {
        startPause.setTag(tag);
        startPause.setImageResource(imgId);
    }

    @Override
    public void addNewLap() {
        if (minText.getText().equals("")){
            listElementsArrayList.add(secText.getText().toString()
                    + "." + milliText.getText().toString());
        }else{
            listElementsArrayList.add(minText.getText().toString() + "." + secText.getText().toString()
                    + "." + milliText.getText().toString());
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public void resetTime() {
        minText.setText("");
        secText.setText("00");
        milliText.setText("000");
    }

    @Override
    public void resetLaps() {
        listElementsArrayList.clear();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setText(String minutes, String seconds, String milliSeconds) {
        if (!minutes.equals("0")){
            minText.setText(minutes + ":");
        }
        secText.setText(seconds);
        milliText.setText(milliSeconds);
    }
}
