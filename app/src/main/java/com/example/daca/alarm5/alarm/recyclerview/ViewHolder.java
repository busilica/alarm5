package com.example.daca.alarm5.alarm.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import com.example.daca.alarm5.R;

/**
 * Created by Daca on 16.03.2017..
 */

public class ViewHolder extends RecyclerView.ViewHolder {

    TextView alarmTime;
    Switch onOffSwitch;
    ImageButton deleteBtn;

    public ViewHolder(View itemView) {
        super(itemView);
        alarmTime = (TextView) itemView.findViewById(R.id.txt_alarm_time_fragment_alarm_custom_row);
        onOffSwitch = (Switch) itemView.findViewById(R.id.switch_fragment_alarm_custom_row);
        deleteBtn = (ImageButton) itemView.findViewById(R.id.imageButton_fragment_alarm_custom_row);
    }
}
