package com.example.daca.alarm5.alarm;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;

import com.example.daca.alarm5.alarm.database.Alarm;
import com.example.daca.alarm5.alarm.database.DBHelper;
import com.example.daca.alarm5.alarm.service.AlarmReceiver;

import java.util.ArrayList;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by Daca on 17.03.2017..
 */

public class AlarmServicePresenter implements Contract.AlarmServicePresenter {

    private AlarmManager alarmManager;
    private Intent intent;
    private PendingIntent pendingIntent;
    private DBHelper dbHelper;
    private Activity activity;

    public AlarmServicePresenter(Activity activity) {
        this.activity = activity;
        alarmManager = (AlarmManager) activity.getSystemService(ALARM_SERVICE);
        intent = new Intent(activity, AlarmReceiver.class);
        dbHelper = new DBHelper(activity);
    }

    @Override
    public void setAlarm(long timeInMillis) {
        intent.putExtra("extra", "alarm on");
        pendingIntent = PendingIntent.getBroadcast(activity, 0, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, timeInMillis, pendingIntent);
    }

    @Override
    public void cancelAlarm() {
        if (pendingIntent == null){
            Intent intent = new Intent();
            intent.putExtra("extra", "alarm off");
            pendingIntent = PendingIntent.getBroadcast(activity, 0, intent,
                            PendingIntent.FLAG_CANCEL_CURRENT); //TODO cancel current?
            alarmManager.cancel(pendingIntent);
            //stop the ringtone
            activity.sendBroadcast(intent);
        }else {
            intent.putExtra("extra", "alarm off");
            alarmManager.cancel(pendingIntent);
            //stop the ringtone
            activity.sendBroadcast(intent);
        }
    }

    @Override
    public void saveAlarm(int alarmHour, int alarmMinute, long mili, int status) {
        dbHelper.insertRow(alarmHour, alarmMinute, mili, 1);
    }

    @Override
    public ArrayList<Alarm> getAllAlarms() {
        return dbHelper.getAllClocks();
    }

    @Override
    public void updateAlarmStatus(int id, int status) {
        dbHelper.updateAlarmStatus(id, status);
    }

    @Override
    public void deleteAlarm(int id) {
        dbHelper.deleteRow(id);
    }
}
