package com.example.daca.alarm5.timer;

import android.os.CountDownTimer;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

/**
 * Created by Daca on 09.03.2017..
 */

class TimerModel implements Contract.TimerModel {

    private CountDownTimer timer;
    private long milliLeft = 0;
    private Contract.TimerPresenter presenter;

    TimerModel(Contract.TimerPresenter presenter) {
        this.presenter = presenter;

    }

    @Override
    public ObservableOnSubscribe<Long> timerStart(final long timeLengthMili) {

        return new ObservableOnSubscribe<Long>() {
            @Override
            public void subscribe(final ObservableEmitter<Long> emmiter) throws Exception {
                timer = new CountDownTimer(timeLengthMili, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        milliLeft = millisUntilFinished;
                        //long min = (millisUntilFinished / (1000*60));
                        //long sec = ((millisUntilFinished / 1000) - min * 60);
                        //presenter.passCountedValues(min, sec);
                        emmiter.onNext(millisUntilFinished);
                    }
                    @Override
                    public void onFinish() {
                        presenter.stopFAB();
                    }
                }.start();
                emmiter.onComplete();
            }
        };


        /*timer = new CountDownTimer(timeLengthMili, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                milliLeft = millisUntilFinished;
                long min = (millisUntilFinished / (1000*60));
                long sec = ((millisUntilFinished / 1000) - min * 60);
                presenter.passCountedValues(min, sec);
            }

            @Override
            public void onFinish() {
                presenter.stopFAB();
            }
        }.start();*/
    }

    @Override
    public void timerPause() {
        timer.cancel();
    }

    @Override
    public void timerResume() {
        timerStart(milliLeft);
    }
}
