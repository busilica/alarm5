package com.example.daca.alarm5.alarm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import java.util.ArrayList;


public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME    =   "alarms.db";
    private static final int    DATABASE_VERSION =   1;
    private static final String TABLE_NAME       =   "alarms";
    private static final String COLUMN_ID        =   "_id";
    private static final String COLUMN_1         =   "hour";
    private static final String COLUMN_2         =   "minute";
    private static final String COLUMN_3         =   "mili";
    private static final String COLUMN_4         =   "onOff";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
       db.execSQL(
               "CREATE TABLE " + TABLE_NAME
               + " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                      + COLUMN_1 + " INTEGER,"
                      + COLUMN_2 + " INTEGER,"
                      + COLUMN_3 + " INTEGER,"
                      + COLUMN_4 + " INTEGER);"
       );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(
                "DROP TABLE IF EXISTS " + TABLE_NAME
        );
        onCreate(db);
    }

    public boolean insertRow(int clockHour, int clockMinute, long mili, int onOff){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_1, clockHour);
        contentValues.put(COLUMN_2, clockMinute);
        contentValues.put(COLUMN_3, mili);
        contentValues.put(COLUMN_4, onOff);
        db.insert(TABLE_NAME, null, contentValues);
        return true;
    }

    public Integer deleteRow(Integer id){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, COLUMN_ID + "=" + id, null);
    }

    public boolean updateRow(int id, int clockHour, int clockMinute, long mili){
        //TODO add edit alarm
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_1, clockHour);
        contentValues.put(COLUMN_2, clockMinute);
        contentValues.put(COLUMN_3, mili);
        db.update(TABLE_NAME, contentValues, COLUMN_ID + "=" + id, null);
        return true;
    }

    public boolean updateAlarmStatus(int id, int onOff){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_3, onOff);
        db.update(TABLE_NAME, contentValues, COLUMN_ID + "=" + id, null);
        return true;
    }

    /*public Model getRow(int rowId){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where "+COLUMN_ID+"="+rowId, null);
        cursor.moveToFirst();
        int id = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
        String title = cursor.getString(cursor.getColumnIndex(COLUMN_1));
        String note = cursor.getString(cursor.getColumnIndex(COLUMN_2));
        int color = cursor.getInt(cursor.getColumnIndex(COLUMN_3));
        Model model = new Model(id, title, note, color);
        return model;
    }*/

    public ArrayList<Alarm> getAllClocks(){
        ArrayList<Alarm> arrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor result = db.rawQuery("select * from " + TABLE_NAME, null);
        result.moveToFirst();
        while (!result.isAfterLast()){
            int id = result.getInt(result.getColumnIndex(COLUMN_ID));
            int clockHour = result.getInt(result.getColumnIndex(COLUMN_1));
            int clockMinute = result.getInt(result.getColumnIndex(COLUMN_2));
            long mili = result.getLong(result.getColumnIndex(COLUMN_3));
            int onOff = result.getInt(result.getColumnIndex(COLUMN_4));
            arrayList.add(new Alarm(id, clockHour, clockMinute, mili, onOff));
            result.moveToNext();
            }
        return arrayList;
    }
}
