package com.example.daca.alarm5.timer;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.example.daca.alarm5.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import san.radialslider.Slider;

/**
 * Created by Daca on 15.03.2017..
 */

public class TimerFragment extends Fragment implements Contract.TimerView {

    private Unbinder unbinder;
    @BindView(R.id.fab_start_fragment_timer)   FloatingActionButton startPause;
    @BindView(R.id.edt_minutes_fragment_timer) EditText minEdt;
    @BindView(R.id.edt_seconds_fragment_timer) EditText secEdt;
    @BindView(R.id.slider_fragment_timer)      Slider slider;

    private Contract.TimerPresenter presenter;
    private Contract.TimerModel model;

    public static TimerFragment newInstance(){
        return new TimerFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_timer, container, false);
        unbinder = ButterKnife.bind(this, fragmentView);
        startPause.setTag(0);
        setupMVP();
        setupSlider();
        startPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                presenter.startPause((int) startPause.getTag());
            }
        });
        return fragmentView;
    }



    @Override
    public void onDestroy() {
        unbinder.unbind();
        presenter = null;
        model = null;
        super.onDestroy();
    }

    @Override
    public void setupFAB(int tag, int imgId) {
        startPause.setTag(tag);
        startPause.setImageResource(imgId);
    }

    @Override
    public long getMin() {
        return (Long.valueOf(minEdt.getText().toString()) * 60) * 1000;
    }

    @Override
    public long getSec() {
        return Long.valueOf(secEdt.getText().toString())* 1000;
    }

    @Override
    public void setEditTexts(long min, long sec) {
        if ((int) min < 10){
            minEdt.setText("0" + String.valueOf(min));
        }else {
            minEdt.setText(String.valueOf(min));
        }
        secEdt.setText(String.valueOf(sec));
    }

    private void setupSlider() {
        slider.registerForSliderUpdates(new Slider.IListenForSliderState() {
            @Override
            public void onSliderMove(float reading) {
                if ((int)reading < 10){
                    minEdt.setText("0" +  String.valueOf((int) reading));
                }else{
                    minEdt.setText(String.valueOf((int) reading));
                }
            }

            @Override
            public void onSliderUp(float reading) {

            }

            @Override
            public void onThumbSelected() {

            }
        });
    }

    private void setupMVP() {
        presenter = new TimerPresenter(this);
        model = new TimerModel(presenter);
        presenter.setModel(model);
    }

    private void hideKeyboard() {
        try{
            InputMethodManager inputMethodManager =
                    (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus()
                    .getWindowToken(), 0);
        }catch (NullPointerException e){

        }

    }
}
