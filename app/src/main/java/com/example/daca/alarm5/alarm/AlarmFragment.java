package com.example.daca.alarm5.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.daca.alarm5.alarm.database.DBHelper;
import com.example.daca.alarm5.R;
import com.example.daca.alarm5.alarm.database.Alarm;
import com.example.daca.alarm5.alarm.recyclerview.Adapter;
import com.example.daca.alarm5.alarm.service.AlarmReceiver;

import java.util.ArrayList;

import static android.content.Context.ALARM_SERVICE;


/**
 * Created by Daca on 15.03.2017..
 */

public class AlarmFragment extends Fragment {

    private View fragmentView;
    private RecyclerView recyclerView;
    private Adapter adapter;
    private Contract.AlarmServicePresenter presenter;



    public static AlarmFragment newInstance(){
        return new AlarmFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_alarm, container, false);
        recyclerView = (RecyclerView) fragmentView.findViewById(R.id.recycler_view_fragment_alarm);
        presenter = new AlarmServicePresenter(getActivity());
        //TODO ONDESTROY
        setupRecyclerView();
        setupFAB();
        return fragmentView;
    }

    private void setupRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (presenter != null ){
            adapter = new Adapter(presenter);
        }
        recyclerView.setAdapter(adapter);
    }

    private void setupFAB() {
        FloatingActionButton addAlarm =
                (FloatingActionButton) fragmentView.findViewById(R.id.fab_add_alarm_fragment_alarm);
        addAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PickAlarmActivity.class);
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            switch (resultCode){
                case 0:
                    Toast.makeText(getContext(), "Canceled", Toast.LENGTH_SHORT).show();
                    break;
                case 2:
                    presenter.setAlarm(data.getLongExtra("timeInMillis", 0));
                    presenter.saveAlarm(data.getIntExtra("alarmHour", 0),
                                        data.getIntExtra("alarmMinute", 0),
                                        data.getLongExtra("timeInMillis", 0),
                                        1);
                    adapter = new Adapter(presenter);
                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);
                    break;
            }
        }
    }
}
