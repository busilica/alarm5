package com.example.daca.alarm5.stopwatch;

/**
 * Created by Daca on 18.03.2017..
 */

public interface Contract {

    interface StopWPresenter {
        void setModel(Contract.StopWModel model);
        void startStop(int tag);
        void passCountedValues(String minutes, String seconds, String milliSeconds);
        long getStartTime();
    }

    interface StopWView{
        void setupFAB(int tag, int imgId);
        void addNewLap();
        void resetTime();
        void resetLaps();
        void setText(String minutes, String seconds, String milliSeconds);
    }

    interface StopWModel {
        void startStopWatch();
        void pauseStopWatch();
        void resetStopWatch();
    }

}
