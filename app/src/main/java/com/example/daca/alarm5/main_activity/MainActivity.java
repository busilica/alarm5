package com.example.daca.alarm5.main_activity;

import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.daca.alarm5.R;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout_activity_main);
        viewPager = (ViewPager) findViewById(R.id.viewPager_activity_main);
        setupTabLayout();
    }

    private void setupTabLayout() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(adapter);
        if (tabLayout != null){
            tabLayout.setBackgroundColor(getColor(R.color.colorPrimary));
            tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#ffff00"));
            tabLayout.setTabTextColors(Color.parseColor("#ffffff"), Color.parseColor("#ffff00"));
            tabLayout.getTabAt(0).setIcon(R.drawable.ic_alarm);
            tabLayout.getTabAt(1).setIcon(R.drawable.ic_timer);
            tabLayout.getTabAt(2).setIcon(R.drawable.ic_stopwatch);
        }
    }
}
