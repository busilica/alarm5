package com.example.daca.alarm5.alarm.recyclerview;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.example.daca.alarm5.alarm.Contract;
import com.example.daca.alarm5.alarm.database.Alarm;
import com.example.daca.alarm5.alarm.database.DBHelper;
import com.example.daca.alarm5.R;
import java.util.ArrayList;


/**
 * Created by Daca on 16.03.2017..
 */

public class Adapter extends RecyclerView.Adapter<ViewHolder> {

    private ArrayList<Alarm> alarmsArrayList;
    private Contract.AlarmServicePresenter presenter;

    public Adapter(Contract.AlarmServicePresenter presenter) {
        this.presenter = presenter;
        alarmsArrayList = presenter.getAllAlarms();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_alarm_custom_row, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Alarm alarm = alarmsArrayList.get(position);
        holder.alarmTime.setText(alarm.getClockHour() + " : " + alarm.getClockMinute());
        switch (alarm.getOnOff()){
            case 0:
                holder.onOffSwitch.setChecked(false);
                break;
            case 1:
                holder.onOffSwitch.setChecked(true);
                break;
        }
        holder.onOffSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    presenter.updateAlarmStatus(alarm.getId(), 1);
                    long mili = alarm.getMili();
                    presenter.setAlarm(mili);
                }else {
                    presenter.updateAlarmStatus(alarm.getId(), 0);
                    presenter.cancelAlarm();
                }
            }
        });
        holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.deleteAlarm(alarm.getId());
                alarmsArrayList = presenter.getAllAlarms();
                notifyDataSetChanged();
                //TODO snackbar
            }
        });
    }

    @Override
    public int getItemCount() {
        return alarmsArrayList.size();
    }
}
