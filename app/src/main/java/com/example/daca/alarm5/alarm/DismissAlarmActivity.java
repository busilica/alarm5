package com.example.daca.alarm5.alarm;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.daca.alarm5.R;
import com.example.daca.alarm5.alarm.service.AlarmReceiver;

public class DismissAlarmActivity extends AppCompatActivity {

    Contract.AlarmServicePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dismiss_alarm);
        Button dismiss = (Button) findViewById(R.id.btn_dismiss_dismiss_activity);
        presenter = new AlarmServicePresenter(this);
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.cancelAlarm();

            }
        });
    }

    @Override
    protected void onDestroy() {
        presenter = null;
        super.onDestroy();
    }
}
