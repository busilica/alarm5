package com.example.daca.alarm5.timer;

import android.widget.Toast;

import com.example.daca.alarm5.R;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Daca on 18.03.2017..
 */

class TimerPresenter implements Contract.TimerPresenter {

    private Contract.TimerView view;
    private Contract.TimerModel timerModel;
    private long min, sec;
    private Observable<Long> observable;

    TimerPresenter(Contract.TimerView view) {
        this.view = view;
    }

    @Override
    public void setModel(Contract.TimerModel model) {
        timerModel = model;
    }

    @Override
    public void startPause(int tag) {
        switch (tag){
            //0 - stopped
            //1 - running
            //3 - resume
            case 0:
                long time = view.getMin() + view.getSec();
                observable = Observable
                        .create(timerModel.timerStart(time))
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnComplete(new Action() {
                        @Override
                        public void run() throws Exception {

                        }
                    });
                observable.subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(Long millisUntilFinished) throws Exception {
                        long lMin = (millisUntilFinished / (1000*60));
                        long lSec = ((millisUntilFinished / 1000) - lMin * 60);
                        view.setEditTexts(lMin, lSec);
                    }
                });
                min = view.getMin();
                sec = view.getSec();
                view.setupFAB(1, R.drawable.fab_stop);
                //timerModel.timerStart(view.getMin() + view.getSec());
                break;
            case 1:
                view.setupFAB(3, R.drawable.fab_start);
                timerModel.timerPause();
                break;
            case 3:
                view.setupFAB(1, R.drawable.fab_stop);
                timerModel.timerResume();
                break;
        }

    }

    @Override
    public void passCountedValues() {
        observable.subscribe(new Consumer<Long>() {
            @Override
            public void accept(Long millisUntilFinished) throws Exception {
                long lMin = (millisUntilFinished / (1000*60));
                long lSec = ((millisUntilFinished / 1000) - lMin * 60);
                view.setEditTexts(lMin, lSec);
            }
        });



    }

    @Override
    public void stopFAB() {
        view.setupFAB(0, R.drawable.fab_start);
        view.setEditTexts(min, sec);
    }
}
