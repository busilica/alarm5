package com.example.daca.alarm5.stopwatch;

import android.os.SystemClock;

import com.example.daca.alarm5.R;

/**
 * Created by Daca on 18.03.2017..
 */

public class StopWPresenter implements Contract.StopWPresenter {

    private Contract.StopWView view;
    private Contract.StopWModel model;
    private long startTime;

    public StopWPresenter(Contract.StopWView view) {
        this.view = view;
    }

    @Override
    public void setModel(Contract.StopWModel model) {
        this.model = model;
    }

    @Override
    public void startStop(int tag) {
        switch (tag){
            case 0:
                startTime = SystemClock.uptimeMillis();
                model.startStopWatch();
                //reset.setEnabled(false); TODO should I?
                view.setupFAB(1, R.drawable.fab_stop);
                break;
            case 1:
                model.pauseStopWatch();
                //reset.setEnabled(true);
                view.setupFAB(0, R.drawable.fab_start);
                break;
            case 2:
                startTime = 0L;
                model.resetStopWatch();
                view.resetTime();
                view.resetLaps();
                break;
            case 3:
                view.addNewLap();
                break;
        }
    }

    @Override
    public void passCountedValues(String minutes, String seconds, String milliSeconds) {
        view.setText(minutes, seconds, milliSeconds);

    }

    @Override
    public long getStartTime() {
        return startTime;
    }
}
