package com.example.daca.alarm5.alarm;

import com.example.daca.alarm5.alarm.database.Alarm;

import java.util.ArrayList;

/**
 * Created by Daca on 17.03.2017..
 */

public interface Contract {

    interface AlarmServicePresenter {
        void setAlarm(long timeInMillis);
        void saveAlarm(int alarmHour, int alarmMinute, long mili, int status);
        ArrayList<Alarm> getAllAlarms();
        void updateAlarmStatus(int id, int status);
        void cancelAlarm();
        void deleteAlarm(int id);
    }

}
