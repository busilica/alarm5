package com.example.daca.alarm5.alarm.service;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.example.daca.alarm5.R;
import com.example.daca.alarm5.alarm.DismissAlarmActivity;
import com.example.daca.alarm5.alarm.PickAlarmActivity;

/**
 * Created by Daca on 14.03.2017..
 */

public class RingtonePlayingService extends IntentService {

    MediaPlayer mediaPlayer;
    //int startId;
    boolean isRunning;

    Ringtone ringtone;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public RingtonePlayingService(String name) {
        super(name);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        //fetch the extra String values
        String stateString = intent.getExtras().getString("extra");

        //this converts the extra Strings from the intent
        //to start IDs, values 0 or 1
        if (stateString != null){
            switch (stateString){
                case "alarm on":
                    startId = 1;
                    break;
                case "alarm off":
                    startId = 0;
                    break;
                default:
                    startId = 0;
                    break;
            }
        }

        //if else statements
        //if there is no music playing, and the user pressed "alarm on"
        //music should start playing
        if (!this.isRunning && startId == 1) {
            Log.e("there is no music,", "and you want to start");
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            ringtone = RingtoneManager.getRingtone(this, uri);
            ringtone.play();
            //create the instance of the media player
            //mediaPlayer = MediaPlayer.create(this, R.raw.alarm);
            //start the ringtone
            //mediaPlayer.setLooping(true);
            //mediaPlayer.start();
            startActivity(new Intent(this, DismissAlarmActivity.class));
            this.isRunning = true;
            startId = 0; //??
        }
        //if there is music playing, and the user pressed "alarm off"
        //music should stop playing
        else if (this.isRunning && startId == 0) {
            Log.e("there is music,", "and you want to stop");
            //stop the ringtone
            //mediaPlayer.stop();
            //mediaPlayer.reset();
            ringtone.stop();
            this.isRunning = false;
            startId = 0;
        }
        //these are if the user presses random buttons
        //if there is no music playing,and the user pressed "alarm off"
        //do nothing
        else if (!this.isRunning && startId == 0) {
            Log.e("there is no music,", "and you want to stop");
            this.isRunning = false;
            startId = 0;
        }
        //if there is music playing and the user pressed "alarm on"
        //do nothing
        else if (this.isRunning && startId == 1) {
            Log.e("there is music,", "and you want to start");
            this.isRunning = true;
            startId = 1;
        }
        //can't think of anyting else, just to catch the odd events
        else{
            Log.e("else", "somehow you reached this");
        }
        return START_NOT_STICKY; //won't restart the service after it stops
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "onDestroy Called", Toast.LENGTH_SHORT).show();
    }
}
