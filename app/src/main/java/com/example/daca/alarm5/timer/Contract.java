package com.example.daca.alarm5.timer;

import android.support.design.widget.FloatingActionButton;

import io.reactivex.ObservableOnSubscribe;

/**
 * Created by Daca on 18.03.2017..
 */

public interface Contract {

    interface TimerModel {
        ObservableOnSubscribe<Long> timerStart(long timeLengthMili);        //starts timer using CountDownTimer class
        void timerPause();                           //pauses timer
        void timerResume();                          //resumes timer
    }

    interface TimerView {
        void setupFAB(int tag, int imgId);           //changes FAB tags, changes FAB icons
        long getMin();                               //returns long value from min editText
        long getSec();                               //returns long value from sec editText
        void setEditTexts(long min, long sec);       //updates editTexts text while timer is running
    }

    interface TimerPresenter {
        void setModel(Contract.TimerModel model);    //I want to null model in onDestroy() fragment
        void startPause(int tag);                    //starts/pauses timerModel, changes FABs icons
        void passCountedValues();  //tells view to change editTexts
        void stopFAB();                              //changes FAB icon to stop, sets editTexts to
    }                                                //original value



}
