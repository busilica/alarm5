package com.example.daca.alarm5.main_activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.daca.alarm5.alarm.AlarmFragment;
import com.example.daca.alarm5.stopwatch.StopwatchFragment;
import com.example.daca.alarm5.timer.TimerFragment;

/**
 * Created by Daca on 15.03.2017..
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragmentTabPosition;
        switch (position){
            case 0:
                fragmentTabPosition = AlarmFragment.newInstance();
                break;
            case 1:
                fragmentTabPosition = TimerFragment.newInstance();
                break;
            case 2:
                fragmentTabPosition = StopwatchFragment.newInstance();
                break;
            default:
                return null;
        }
        return fragmentTabPosition;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        CharSequence title;
        switch (position){
            case 0:
                title = "Alarm";
                break;
            case 1:
                title = "Timer";
                break;
            case 2:
                title = "Stopwatch";
                break;
            default:
                return null;
        }
        return title;
    }
}
